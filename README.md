AlSoMe: A Demonstration of a High Available Chat App
===

This project is a demonstration of how horizontally scaled servers can
provide high availability. It is also a demonstration of Dockerfiles
and Swarm compose files.

The domain is a simple and crude social media *Chat App*, in which
people write and share chats, similar to SnapChat, Messenger, etc.

![AlSoMe Chat Window](resources/alsome-demo.jpg)

For ease of deployment, the chat app is a browser based single-page
app (SPA).

The name *AlSoMe* may either be short for *Alternative Social Media* or
*(I) Also (want to build a) Social Media*.

License
===

You may use this project under the [Apache 2](LICENSE) license.

What is this repository for?
===

The project is meant as teaching and demonstration material, and an
  example of how to use Docker Swarm for deploying a horizontally
  scaled system of stateless servers, which allows one or more servers
  to go off-line while the system keeps processing. As such it is an
  example of the **Bulkhead** pattern from Michael T. Nygard's book
  *Release It*.

Security and privacy is ignored for the sake of code simplicity.

The AlSoMe server is configurable so you may run in a development mode
using in-memory storage (naturally only a single server is supported
then), or in production with Redis as storage tier.

Prerequisites
----

For demonstration and deployment, a Docker engine and Docker Swarm
installation is required. Knowledge of these tools is required.

For development, the project assumes a Java JDK 11 and Gradle 6.8
development environment.

How do I get set up?
===

For simple demonstration (no development) you just need [Docker
engine](https://docs.docker.com/engine/install/) installed.

Single-server in-memory app
---

To run a single server in-memory chat app, just use the public docker
image:

    docker run -d -p 5201:5201 henrikbaerbak/alsome-jar:v2

Next, you should browse to [the AlSoMe landing
page](http://localhost:5201) deployed on *localhost:5201*.

You will be presented with the welcome page. Just enter any user name
(no authentication process is used) and hit enter.

![AlSoMe welcome page](resources/alsome-welcome.jpg)

Next hit the 'Go to Chat' button to get to the main chat room. Enter
any text, and you will see it in the chat message board.

![AlSoMe chat page](resources/alsome-chat.jpg)

Multi-server Redis-based app
---

The above setup of course does not demonstrate the high availability
feature of the app. For that, you have to deploy a stack using docker
swarm in which each server is stateless, and state is stored in a
common database;.

For testing, ensure your machine is running Docker engine in swarm
mode and then

    docker stack deploy -c alsome-stack.yml alsomestack
    
This will deploy three AlSoMe instances (again accessible on port
5201), a single Redis database, and the [Docker
*visualizer*](http://localhost:8080) on port 8080.

To view the stack, use the normal

    docker stack ps alsomestack

To demonstrate the high availability of the system, you of course
*need a swarm consisting of at least two nodes/machines* and ideally
with a *public IP address/DNS* for a public audience.

Let the audience enter chat messages while you kill and restart
individual nodes of the swarm ala:

    docker node update --availability drain (node-name)

and
    
    docker node update --availability active (node-name)


    
For my own demonstrations I used four VM's on Digital Ocean joined in
a swarm, and assigned DNS to each node (see the initial
screenshot above). Thus, the audience may access the AlSoMe Chat App on any
of the four DNS.

### Development overview ###

The project is Java 11 + Gradle 6.8 based and uses Apache Velocity as template
engine. The style sheets are from the [W3 Mobile App
tutorial](https://www.w3schools.com/w3css/w3css_mobile.asp).

SLF4J is used for logging, and JUnit 5 and Hamcrest for testing.

A multistage docker file is included, so to build the image, just issue

    docker build -f Dockerfile-multistage -t alsome-jar .
    
You can thus build the image without installing Java and Gradle.

### Version 2 ###

Version 2 of AlSoMe uses AntiSamy to sanitize login names and chat
messages to avoid some forms of attack.

### Who do I talk to? ###

For more information, please contact *Henrik Bærbak Christensen* at
  `hbc at cs dot au dot dk`. I would love to hear, if you find it
  usefull for your learning or teaching.
