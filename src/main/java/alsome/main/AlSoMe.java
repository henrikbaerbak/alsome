/*
 * Copyright (C) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package alsome.main;

import java.util.*;

import static spark.Spark.*;

import alsome.domain.*;
import alsome.redis.RedisChatStore;
import alsome.redis.RedisUserIndex;
import alsome.sanitize.VictorCleaningMan;
import org.owasp.validator.html.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

/** Main class for the AlSoMe server, exposing
 * its web front end on port 5201.
 *
 * Takes a single argument, namely the
 * hostname of the redis storage node. If that host
 * equals 'fake' then an in-memory database is used.
 * Otherwise port 6379 is used on that particular node.
 * */

public class AlSoMe {
  // Outer layouts
  public static final String LANDING_PAGE_LAYOUT = "templates/landingpage.vtl";
  public static final String CHAT_PAGE_LAYOUT = "templates/chatpage.vtl";
  // Inner layouts
  public static final String LOGIN_INNER_LAYOUT = "templates/login.vtl";
  public static final String WELCOME_INNER_LAYOUT = "templates/welcome.vtl";
  public static final String CHAT_INNER_LAYOUT = "templates/chat.vtl";
  public static final String CHAT_OFFLINE_LAYOUT = "templates/offline.vtl";

  private final Logger logger;
  private final VictorCleaningMan victor;
  private User user;
  private final UserIndex userIndex;
  private final ChatStore chatStore;

  public static void main(String[] args) throws PolicyException {
    AlSoMe main = new AlSoMe(args);
    main.registerRoutes();
  }

  public AlSoMe(String[] args) throws PolicyException {
    logger = LoggerFactory.getLogger("AppServerMain");

    user = null;
    if (args.length != 1) printHelp();
    String host = args[0];

    logger.info("AlSoMe starting on DB " + host);

    if (host.equals("fake")) {
      userIndex = new FakeUserIndex();
      chatStore = new FakeChatStore();
    } else {
      String[] split = host.split(":");
      userIndex = new RedisUserIndex(split[0], Integer.parseInt(split[1]));
      chatStore = new RedisChatStore(split[0], Integer.parseInt(split[1]));
    }

    victor = new VictorCleaningMan();
  }

  private void printHelp() {
    System.out.println("AlSoMe: A simple chat service on port 5201");
    System.out.println("  Usage: AlSoMe {dbhost}");
    System.out.println("  dbhost: either 'fake', otherwise redis port ala 'localhost:6379'");
    System.exit(-1);
  }

  private void registerRoutes() {
    port(5201);

    // ================= Intro screen with login form
    // Submit -> /login
    get("/", (request, response) -> {
      logger.info("method=GET, path=/");

      Map<String, Object> model = new HashMap<String, Object>();
      model.put("template", LOGIN_INNER_LAYOUT);

      return new ModelAndView(model, LANDING_PAGE_LAYOUT);
    }, new VelocityTemplateEngine());

    // === Invoked with username signaling user login attempt
    post("/login", (request, response) -> {
      String usernameraw = request.queryParams("username").toLowerCase();
      // sanitize input
      String username = sanitize(usernameraw, 8);

      logger.info("method=POST, path=/login, username={}", username);
      // Try to find user in userIndex
      user = userIndex.lookup(username);
      logger.info("trying to find user: {}", user);
      if (user == null) {
        logger.info("No such user, just creating one");
        user = userIndex.createUser(username);
      }

      Map<String, Object> model = new HashMap<>();
      model.put("template", WELCOME_INNER_LAYOUT);
      // The provided username
      model.put("username", user.getUsername());
      model.put("userid", user.getId());
      model.put("colorString", user.getColorString());

      return new ModelAndView(model, LANDING_PAGE_LAYOUT);
    }, new VelocityTemplateEngine());

    // === The main chat page, landing page for GET requests
    get("/chat/:userid", (request, response) -> {
      String userid = request.params(":userid");
      logger.info("GOT UID: {}", userid);
      User user = userIndex.lookupOnId(userid);
      if (user == null) {
        response.redirect("/");
      }
      logger.info("method=GET, context=/chat");
      Map<String, Object> model = new HashMap<>();
      model.put("template", CHAT_INNER_LAYOUT);
      model.put("username", user.getUsername());
      model.put("userid", user.getId());
      model.put("colorString", user.getColorString());

      List<Chat> chatList = chatStore.getLastNMessages(ChatStore.N);
      model.put("chatList", chatList);

      return new ModelAndView(model, CHAT_PAGE_LAYOUT);
    }, new VelocityTemplateEngine());

    // === Internal refresh messages for the inner chat contents
    // triggered by the javascript embedded in the page
    get("/chat/contents/:userid", (request, response) -> {
      String userid = request.params(":userid");
      logger.info("INNER GOT UID: {}", userid);
      User user = userIndex.lookupOnId(userid);
      if (user == null) {
        Map<String, Object> model = new HashMap<>();
        model.put("template", CHAT_OFFLINE_LAYOUT);
        logger.info(" Ups, no such user, showing offline screen");
        return new ModelAndView(model, CHAT_OFFLINE_LAYOUT);
      }
      logger.info("method=GET(Inner), context=/chat/contents");

      Map<String, Object> model = new HashMap<>();
      List<Chat> chatList = chatStore.getLastNMessages(ChatStore.N);
      model.put("chatList", chatList);

      return new ModelAndView(model, CHAT_INNER_LAYOUT);
    }, new VelocityTemplateEngine());

    // Landing page for ADDING new chats; only accepts if sent
    // chat is non-empty
    post("/chat/:userid", (request, response) -> {
      String userid = request.params(":userid");
      User user = userIndex.lookupOnId(userid);
      if (user == null) {
        response.redirect("/");
      }

      String chatMsgDirty = request.queryParams("chat");
      String chatMsg = sanitize(chatMsgDirty, 80);
      logger.info("method=POST, context=/chat, msg={}", chatMsg);
      if (! chatMsg.equals("")) {
        chatStore.addChat(new Chat(user.getUsername(), user.getColorString(), chatMsg));
      }

      Map<String, Object> model = new HashMap<>();
      model.put("template", CHAT_INNER_LAYOUT);
      model.put("username", user.getUsername());
      model.put("userid", user.getId());
      model.put("colorString", user.getColorString());

      List<Chat> chatList = chatStore.getLastNMessages(ChatStore.N);
      model.put("chatList", chatList);
      return new ModelAndView(model, CHAT_PAGE_LAYOUT);
    }, new VelocityTemplateEngine());
  }

  private String sanitize(String dirtyInput, int maxLength) {
    return victor.sanitize(dirtyInput, maxLength);
  }

}