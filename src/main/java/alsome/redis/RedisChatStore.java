/*
 * Copyright (C) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package alsome.redis;

import alsome.domain.Chat;
import alsome.domain.ChatStore;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** Redis backed implementation of the ChatStore */
public class RedisChatStore implements ChatStore {
  public static final String CHAT_COUNTER_KEY = "CHAT-COUNTER";
  private final JedisPool jedisPool;
  private final Logger logger;
  private final Gson gson;

  public RedisChatStore(String host, int port) {
    jedisPool = new JedisPool(host, port);
    logger = LoggerFactory.getLogger(RedisChatStore.class);
    gson = new Gson();

    // Initialize chat counter
    try (Jedis jedis = jedisPool.getResource()) {
      if (! jedis.exists(CHAT_COUNTER_KEY))
        jedis.set(CHAT_COUNTER_KEY, "0");
    }
  }

  @Override
  public List<Chat> getLastNMessages(int n) {
    logger.info("method=getLastNMessages, n={}", n);

    List<Chat> list = new ArrayList<>();
    try (Jedis jedis = jedisPool.getResource()) {
      // Get the index of last stored message.
      long last = Long.parseLong(jedis.get(CHAT_COUNTER_KEY));

      // Handle empty list case
      if (last == 0) return list;

      // Generate list of keys for last n messages,
      // note that redis stores first message as no 1,
      // not as no 0!

      // Compute the index of first message in list
      long lowerLimit = last-n+1 >= 1 ? last-n+1 : 1;
      List<String> keyList = new ArrayList<>();
      for (long i = lowerLimit; i <= last; i++) {
        keyList.add(computeChatKey(i));
      }
      // Cumbersome conversion to Array
      String[] asArray = new String[keyList.size()];
      keyList.toArray(asArray);

      // Finally, get the list of json chats
      List<String> jsonChatList = jedis.mget(asArray);
      // and convert back to chats
      list = jsonChatList.stream()
              .map( json -> {
                Chat chat = gson.fromJson(json, Chat.class);
                return chat;
              })
              .collect(Collectors.toList());
    }
    return list;
  }

  @Override
  public void addChat(Chat chat) {

    try (Jedis jedis = jedisPool.getResource()) {
      long index = jedis.incr(CHAT_COUNTER_KEY);
      logger.info("method=addChat, chat={}, primary-id={}", chat, index);
      String asJsonString = gson.toJson(chat);
      jedis.set(computeChatKey(index), asJsonString);
    }
  }

  private String computeChatKey(long chatIndex) {
    return "CHAT-" + chatIndex;
  }
}
