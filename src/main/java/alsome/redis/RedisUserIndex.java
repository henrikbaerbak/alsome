/*
 * Copyright (C) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package alsome.redis;

import alsome.domain.FakeUserIndex;
import alsome.domain.User;
import alsome.domain.UserIndex;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/** Redis backed implementation of the UserIndex */
public class RedisUserIndex implements UserIndex {
  public static final String USER_KEY = "USER-";
  // Important that it is not named 'USER-COUNTER' as I use
  // a key lookup when searching for users!
  public static final String USER_COUNTER_KEY = "COUNTER-USER";
  private final JedisPool jedisPool;
  private final Logger logger;
  private final Gson gson;
  private String[] colorTable;

  public RedisUserIndex(String host, int port) {
    jedisPool = new JedisPool(host, port);
    // TODO: unholy reuse
    colorTable = FakeUserIndex.createColorTable();
    logger = LoggerFactory.getLogger(RedisUserIndex.class);
    gson = new Gson();

    // Initialize user counter
    try (Jedis jedis = jedisPool.getResource()) {
      if (! jedis.exists(USER_COUNTER_KEY))
        jedis.set(USER_COUNTER_KEY, "0");
    }

  }

  @Override
  public User lookup(String username) {
    User user = null;

    // TODO: A bit clumsy
    try (Jedis jedis = jedisPool.getResource()) {
      Set<String> allUsers = jedis.keys(USER_KEY+"*");
      String userKey = allUsers.stream().filter( key  -> {
        String value = jedis.get(key);
        User x = gson.fromJson(value, User.class);
        return x.getUsername().equals(username);
      }).findFirst().orElse(null);
      if ( userKey != null) {
        return gson.fromJson(jedis.get(userKey), User.class);
      }
    }
    return null;
  }

  @Override
  public User lookupOnId(String userid) {
    User user = null;
    try (Jedis jedis = jedisPool.getResource()) {
      String userAsJson = jedis.get(computeUserKey(userid));
      user = gson.fromJson(userAsJson, User.class);
    }
    return user;
  }

  private String computeUserKey(String userid) {
    return USER_KEY + userid;
  }

  @Override
  public synchronized User createUser(String username) {
    User user = null;
    try (Jedis jedis = jedisPool.getResource()) {
      long asLong = jedis.incr(USER_COUNTER_KEY);
      int userIndex = (int) asLong;
      int index = userIndex % colorTable.length;
      String colorString = colorTable[index];
      logger.info("Creating user {} with index {} and color {}", username, index, colorString);
      user = new User(username, colorString, "u"+userIndex);
      String asJsonString = gson.toJson(user);
      jedis.set(computeUserKey(user.getId()), asJsonString);
    }
    return user;
  }
}
