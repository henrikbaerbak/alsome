package alsome.sanitize;

import org.owasp.validator.html.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

public class VictorCleaningMan {
  private final AntiSamy as;
  private final Policy policy;
  private final Logger logger;

  public VictorCleaningMan() throws PolicyException {
    URL highPolicyURL = getClass().getClassLoader().getResource("antisamy-slashdot.xml");
    policy = Policy.getInstance(highPolicyURL);
    as = new AntiSamy();
    logger = LoggerFactory.getLogger(VictorCleaningMan.class);
  }

  public String sanitize(String dirtyInput, int maxLength) {
    String sanitizedString = "";
    CleanResults cr;
    try {
      cr = as.scan(dirtyInput, policy);
      sanitizedString = cr.getCleanHTML();
    } catch (ScanException e) {
      logger.error("method=sanitize, result=scanException", e);
    } catch (PolicyException e) {
      logger.error("method=sanitize, result=policyException", e);
    }
    // avoid
    if (sanitizedString.length() == 0) {
      if (maxLength< 10)
        return "baddie";
      else
        return "script attacks not allowed, says AlSoMe";
    }
    return sanitizedString.substring(0,
            (sanitizedString.length()<maxLength ? sanitizedString.length() : maxLength));
  }
}
