/*
 * Copyright (C) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package alsome.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/** In memory implementation of the UserIndex. Also holds
 * a user-color-assignment algorithm.
 * */
public class FakeUserIndex implements UserIndex {
  private final Logger logger;
  private Map<String, User> userDb;
  public FakeUserIndex() {
    userDb = new HashMap<>();
    logger = LoggerFactory.getLogger(FakeUserIndex.class);
    colorTable = createColorTable();
  }

  public static String[] createColorTable() {
    String[] colorTable = new String[4 * 4 * 4];
    String values[] = new String[]{"3f", "bf", "7f", "00"};
    int N = values.length;
    int counter = 0;
    for (int red = 0; red < N; red++)
      for (int green = 0; green < N; green++)
        for (int blue = 0; blue < N; blue++) {
          String color = "#" + values[red] + values[green] + values[blue];
          colorTable[counter++] = color;
        }
    return colorTable;

  }

  @Override
  public User lookup(String username) {
    String keyOfUser = userDb.keySet().stream()
            .filter( key -> userDb.get(key).getUsername().equals(username))
            .findFirst()
            .orElse(null);
    if (keyOfUser != null) return lookupOnId(keyOfUser);
    return null;
  }

  @Override
  public User lookupOnId(String userid) {
    return userDb.get(userid);
  }

  @Override
  public User createUser(String username) {
    int index = userDb.size() % colorTable.length;
    String colorString = colorTable[index];
    logger.info("Creating user {} with index {} and color {}", username, index, colorString);
    User user = new User(username, colorString);
    userDb.put(user.getId(), user);
    return user;
  }

  private static String[] colorTable;
}
