/*
 * Copyright (C) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package alsome.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** In memory implementation of the ChatStore */
public class FakeChatStore implements ChatStore {
  private final List<Chat> list;

  public FakeChatStore() {
    list = Collections.synchronizedList(new ArrayList<>());
  }

  @Override
  public List<Chat> getLastNMessages(int n) {
    return list.subList(Math.max(list.size()-n, 0), list.size());
  }

  @Override
  public void addChat(Chat chat) {
    list.add(chat);
  }
}
