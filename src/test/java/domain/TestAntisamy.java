package domain;

import alsome.sanitize.VictorCleaningMan;
import org.junit.jupiter.api.Test;
import org.owasp.validator.html.*;

import java.net.URL;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestAntisamy {
  @Test
  public void shouldSanitizeInput() throws PolicyException, ScanException {
    URL highPolicyURL = getClass().getClassLoader().getResource("antisamy-slashdot.xml");
    Policy policy = Policy.getInstance(highPolicyURL);
    AntiSamy as = new AntiSamy();

    // When I enter text with emoji's
    String dirtyInput = "Hej med dig. \uD83D\uDE00 - \uD83E\uDD70 - \uD83D\uDE09";
    CleanResults cr = as.scan(dirtyInput, policy);
    // Then they are accepted
    assertThat(cr.getCleanHTML(), is(dirtyInput));

    // When I add javascript
    dirtyInput = "Fisk <script>alert('idiot')</script>";
    cr = as.scan(dirtyInput, policy);
    // Then they are filtered out
    assertThat(cr.getCleanHTML(), is("Fisk"));
    //System.out.println("--> " + cr.getErrorMessages());

    dirtyInput = "Fisk <h1>alert('idiot')</h1>";
    cr = as.scan(dirtyInput, policy);
    // Then they are filtered out
    assertThat(cr.getCleanHTML(), is("Fisk alert('idiot')"));
    //System.out.println("--> " + cr.getErrorMessages());
  }

  @Test
  public void shouldSanitizeUsingVictor() throws PolicyException {
    VictorCleaningMan victor = new VictorCleaningMan();
    assertThat(victor.sanitize("victor", 8), is("victor"));
    assertThat(victor.sanitize("victor the cleaning man", 8), is("victor t"));
    assertThat(victor.sanitize("hej <h1>fisk</h1>      ", 90), is("hej fisk"));
    assertThat(victor.sanitize("ej med dig. \uD83D\uDE00 - \uD83E\uDD70 - \uD83D\uDE09      ", 90),
            is("ej med dig. \uD83D\uDE00 - \uD83E\uDD70 - \uD83D\uDE09"));

    assertThat(victor.sanitize("<script>alert('idito')</script>", 90),
            is("script attacks not allowed, says AlSoMe"));

    assertThat(victor.sanitize("<script>alert('idito')</script>", 8),
            is("baddie"));
  }
}
