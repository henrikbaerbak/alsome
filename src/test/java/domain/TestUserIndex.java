/*
 * Copyright (C) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package domain;

import alsome.domain.FakeUserIndex;
import alsome.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestUserIndex {
  private FakeUserIndex userIndex;

  @BeforeEach
  public void setup() {
    userIndex = new FakeUserIndex();
  }

  @Test
  public void shouldCreateAndRetrieveUser() {
    // Given a user index
    // When I create a user
    User user1 = userIndex.createUser("hbc");
    // Then it is valid
    assertThat(user1, is(notNullValue()));
    assertThat(user1.getUsername(), is("hbc"));
    assertThat(user1.getId(), is(notNullValue()));
    assertThat(user1.getColorString(), is(notNullValue()));

    User user2 = userIndex.createUser("benny");
    assertThat(user2.getId(), is(not(user1.getId())));

    user1 = userIndex.lookup("hbc");
    assertThat(user1.getUsername(), is("hbc"));

    User user1Copy = userIndex.lookupOnId(user1.getId());
    assertThat(user1.getColorString(), is(user1Copy.getColorString()));
    assertThat(user1.getUsername(), is(user1Copy.getUsername()));
    assertThat(user1.getId(), is(user1Copy.getId()));
  }
}