/*
 * Copyright (C) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package domain;

import alsome.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

public class TestChatStore {
  private ChatStore chatStore;
  private UserIndex userIndex;
  private User magnus;
  private User mathilde;

  @BeforeEach
  public void setup() {
    chatStore = new FakeChatStore();
    userIndex = new FakeUserIndex();
    magnus = userIndex.createUser("magnus");
    mathilde = userIndex.createUser("mathilde");
  }

  @Test
  public void shouldChatWorkYesItMust() {
    // Given three chats
    Chat chat1 = new Chat(magnus, "test1"),
            chat2 = new Chat(magnus, "test2"),
            chat3 = new Chat(mathilde, "test3");
    // When I enter them in the store
    chatStore.addChat(chat1);
    chatStore.addChat(chat2);
    chatStore.addChat(chat3);
    // Then I can retrieve them again
    List<Chat> list = chatStore.getLastNMessages(ChatStore.N);
    assertThat(list.size(), is(3));
    assertThat(list.get(0).getChatMsg(), is("test1"));
    assertThat(list.get(0).getUsername(), is("magnus"));
    assertThat(list.get(2).getChatMsg(), is("test3"));
    assertThat(list.get(2).getUsername(), is("mathilde"));

    // When I add to a total of 9 msg
    chatStore.addChat(chat1);
    chatStore.addChat(chat2);
    chatStore.addChat(chat3);
    chatStore.addChat(chat1);
    chatStore.addChat(chat2);
    chatStore.addChat(new Chat(magnus, "Final"));

    // I only get N back
    final int N = 6;
    list = chatStore.getLastNMessages(N);
    assertThat(list.size(), is(N));
    assertThat(list.get(N-1).getChatMsg(), is("Final"));
    assertThat(list.get(N-1).getUsername(), is("magnus"));
  }
}
