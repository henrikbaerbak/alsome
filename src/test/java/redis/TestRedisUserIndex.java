/*
 * Copyright (C) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package redis;

import alsome.domain.User;
import alsome.domain.UserIndex;
import alsome.redis.RedisUserIndex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/** Manual integration test of Redis backend.
 * Assumes a redis is running on localhost:6379:
 *
 * docker run -d --name redis -p 6379:6379 redis:6.2.5-alpine
 * docker exec -ti redis redis-cli
 *
 * Between runs, use 'flushall' in redis OR
 * if run multiple times, review that each
 * user is associated with new colours; and
 * that counter is incremented.
 *
 *
 */
@Disabled
public class TestRedisUserIndex {
  private UserIndex userIndex;

  @BeforeEach
  public void setup() {
    userIndex = new RedisUserIndex("localhost", 6379);
  }

  // TODO: duplicate from the fake version.
  @Test
  public void shouldCreateAndRetrieveUser() {
    // Given a user index
    // When I create a user
    User user1 = userIndex.createUser("hbc");
    // Then it is valid
    assertThat(user1, is(notNullValue()));
    assertThat(user1.getUsername(), is("hbc"));
    assertThat(user1.getId(), is(notNullValue()));
    assertThat(user1.getColorString(), is(notNullValue()));

    User user2 = userIndex.createUser("benny");
    assertThat(user2.getId(), is(not(user1.getId())));

    user1 = userIndex.lookup("hbc");
    assertThat(user1.getUsername(), is("hbc"));

    User user1Copy = userIndex.lookupOnId(user1.getId());
    assertThat(user1.getColorString(), is(user1Copy.getColorString()));
    assertThat(user1.getUsername(), is(user1Copy.getUsername()));
    assertThat(user1.getId(), is(user1Copy.getId()));
  }

}
